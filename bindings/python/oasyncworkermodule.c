#include <pygobject.h>
 
void oasyncworker_register_classes (PyObject *d); 
extern PyMethodDef oasyncworker_functions[];
 
DL_EXPORT(void)
initoasyncworker(void)
{
    PyObject *m, *d;
 
    init_pygobject ();
 
    m = Py_InitModule ("oasyncworker", oasyncworker_functions);
    d = PyModule_GetDict (m);
 
    oasyncworker_register_classes (d);
 
    if (PyErr_Occurred ()) {
        Py_FatalError ("can't initialise module oasyncworker");
    }
}
