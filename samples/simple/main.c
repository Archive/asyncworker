#include <glib.h>
#include <oasyncworker/oasyncworker.h>

enum {
	PRIORITY_HIGH = 100,
	PRIORITY_NORMAL = 50,
	PRIORITY_LOW = 0,
};


static void
dummy_function (OAsyncWorkerTask *task)
{
	o_async_worker_task_cancel_point (task, TRUE);
}

static gpointer
dummy_func (OAsyncWorkerTask *task, gpointer arguments)
{

	dummy_function (task);
	
	g_print ("Dummy func had priority of %d with arguments: (%s)\n", 
		o_async_worker_task_get_priority (task),
		(gchar*) arguments);

	
	o_async_worker_task_cancel_point (task, TRUE);
		
	return (gpointer)g_strdup ("Also free me!");
}

static void
dummy_callback (OAsyncWorkerTask *task, gpointer func_result)
{
	gchar *arguments = o_async_worker_task_get_arguments (task);

	g_print ("Dummy callback going to free (%s) and (%s)\n", 
		(gchar*)arguments, (gchar*)func_result);

	g_free (arguments);
	
	if (!o_async_worker_task_is_cancelled(task))
		g_free (func_result);
}

static void
simple_add_dummy_task (OAsyncWorker *queue, gint priority, gint number)
{
	static gboolean cancel;
	
	OAsyncWorkerTask *dummy = o_async_worker_task_new ();

	o_async_worker_task_set_arguments (dummy, g_strdup_printf ("Number %d: free me!", number));
	o_async_worker_task_set_func (dummy, dummy_func);
	o_async_worker_task_set_callback (dummy, dummy_callback);
	o_async_worker_task_set_priority (dummy, priority);
	
	o_async_worker_add (queue, dummy);
	
	g_object_unref (dummy);

	if (cancel)
	{
		o_async_worker_task_request_cancel (dummy);
		cancel = FALSE;
	} else {
		cancel = TRUE;
	}
}

static void
queue_task_handler (OAsyncWorker *queue, gint task_id, gpointer user_data)
{
	g_print ("Task with id %d %s\n", task_id, (const gchar*)user_data);
}


int
main (int argc, char **argv)
{
	gint i = 0;
	OAsyncWorker *queue;
	GMainLoop *mainloop = g_main_loop_new (NULL, FALSE);
 
	g_type_init ();

	queue = o_async_worker_new ();

	g_signal_connect (G_OBJECT (queue), "task_finished", G_CALLBACK(queue_task_handler), "finished");
	g_signal_connect (G_OBJECT (queue), "task_added", G_CALLBACK(queue_task_handler), "added");
	g_signal_connect (G_OBJECT (queue), "task_removed", G_CALLBACK(queue_task_handler), "removed");
	
	for (i=0; i < 100; i++)
	{
		g_print (".");
		simple_add_dummy_task (queue, i, i);
		o_async_worker_add_wait (queue, 100000, PRIORITY_NORMAL);
	}

	simple_add_dummy_task (queue, PRIORITY_HIGH+200, 101);
	simple_add_dummy_task (queue, PRIORITY_HIGH+0, 102);
	simple_add_dummy_task (queue, PRIORITY_HIGH+100, 103);
	simple_add_dummy_task (queue, PRIORITY_HIGH+300, 104);

	g_print ("\n");

	g_main_loop_run (mainloop);
	
	o_async_worker_join (queue);

	g_object_unref (queue);
}
