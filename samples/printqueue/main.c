/* Printqueue
 * Copyright (C) 2005 Nicolas Trangez <eikke@eikke.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <glib.h>
#include <oasyncworker/oasyncworker.h>

struct _PrintJob
{
        guint id;
        guint pages;
};

typedef struct _PrintJob PrintJob;

static void
add_print_job_to_queue(OAsyncWorker *queue, guint num_pages);

static GMainLoop *loop = NULL;

/* This is our job function. Many of these will be queued, with different data,
 * and will be run in a separate thread */
static gpointer
print_pages(OAsyncWorkerTask *task, gpointer data)
{
        guint cnt = 0;
        PrintJob *job = (PrintJob *) data;

        /* I got a wrong function prototype in the beginning, and am too lazy to retype everything */
        guint task_num = job->id;
        guint num_pages = job->pages;
        
        g_print("[job %d] Initializing printer, this takes 3 seconds...\n", task_num);

        g_usleep(3 * G_USEC_PER_SEC);

        g_print("[job %d] Initializing printer done\n", task_num);

        for(cnt = 0; cnt < num_pages; cnt++)
        {
                g_print("[job %d] Printing page %d of %d, this will take 2 seconds\n", task_num, cnt + 1, num_pages);
                g_usleep(2 * G_USEC_PER_SEC);
        }

        g_print("\n[job %d] Job finished, go get your lecture\n\n", task_num);

        return NULL;
}

/* Our timeout function.
 * Show the user our main program is still running, and launch a new
 * print job once in a while */
static gboolean
timeout(gpointer d)
{
        static int counter = 0;
        guint32 pages = 0;
        static GRand *rand = NULL;

        if(counter > 10) {
                g_print("\n\n[main] Got %d timeouts, stopping main program\n", counter);
                g_print("[main] Remaining queue tasks will be processed first though before we exit.\n\n");

                g_main_loop_quit(loop);

                return FALSE;
        }

        if(rand == NULL)
        {
                rand = g_rand_new();
        }

        g_print("\n\t[main] Timeout: %d, program is running fine :-)\n", counter);

        if(counter % 3 == 0)
        {
                pages = g_rand_int_range(rand, 2, 5);
                g_print("[main] count % 3 == 0, adding new job with %d pages\n");
                add_print_job_to_queue(O_ASYNC_WORKER(d), pages);
        }
                
        counter++;
        return TRUE;
}

/* d is the return value of the job function, always NULL here */
static void
free_data(OAsyncWorkerTask *i, gpointer d)
{
        PrintJob *j = NULL;

        /* Free the data given to the job function */
        /* Retrieve it */
        j = (PrintJob *) o_async_worker_task_get_arguments(i);
        g_print("\n[main] Freeing up data for job %d\n", j->id);
        /* And free */
        if(j != NULL)
        {
                g_free(j);
        }
        
        if(d != NULL)
        {
                /* This should not ever happen, because our print function
                 * always returns NULL */
                g_free(d);
                g_assert_not_reached();
        }
}

static void
add_print_job_to_queue(OAsyncWorker *queue, guint num_pages)
{
        /* We need to keep track of the task number */
        static int task_num = 0;
        /* We want fair queueing, so we need to give priorities to jobs.
         * Otherwise job picking is random. */
        static int priority = 50;
        /* One queue task (a "job") */
        OAsyncWorkerTask *task = NULL;
        /* Job description */
        PrintJob *job = NULL;

        g_return_if_fail(queue != NULL);
        
        /* Create a new queue task */
        task = o_async_worker_task_new();
        g_assert(task != NULL);

        /* Create a new print job representation */
        job = g_new0(PrintJob, 1);
        g_assert(job != NULL);

        /* Set the job properties */
        job->id = task_num;
        task_num++;
        job->pages = num_pages;

        /* Set the queue task properties */
        /* The data to pass */
        o_async_worker_task_set_arguments(task, (gpointer) job);
        /* The function to call */
        o_async_worker_task_set_func(task, print_pages);
        /* The function to call when the previous function returns */
        o_async_worker_task_set_callback(task, free_data);
        /* Set task priority */
        o_async_worker_task_set_priority(task, priority);
        /* Increment the priority to get fair queueing */
        priority--;

        g_print("\n[main] Adding print job %d with %d pages\n", job->id, job->pages);

        /* Add the job to the queue */
        o_async_worker_add(queue, task);
	g_object_unref (task);
}

gint
main(guint argc, gchar *argv)
{
        /* Or queue */
        OAsyncWorker *queue = NULL;
        
        g_type_init();

        /* Create the main loop */
        loop = g_main_loop_new(NULL, FALSE);
        g_assert(loop != NULL);

        /* Create a new OAsyncWorker instance */
        queue = o_async_worker_new();
        g_assert(queue != NULL);

        /* Add our timeout func, to show the user or main "program" is still running */
        g_timeout_add(3 * 1000, timeout, queue);

        /* Add some printer task */
        add_print_job_to_queue(queue, 1);
        add_print_job_to_queue(queue, 2);
        add_print_job_to_queue(queue, 3);

        g_print("\nStarting mainloop\n");
        g_print("Please let me run for a while, I will end, then inspect my output\n\n");

        /* Start our mainloop */
        g_main_loop_run(loop);

        o_async_worker_join(queue);

        g_print("\n");

        g_object_unref(queue);
}
