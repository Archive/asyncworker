#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib.h>
#include <oasyncworker/oasyncworker.h>

typedef struct _treequeue_t treequeue_t;

struct _treequeue_t
{
	GtkTreeView *treeview;
	OAsyncWorker *queue;
};


enum {
	PRIORITY_HIGH = 100,
	PRIORITY_NORMAL = 50,
	PRIORITY_LOW = 0,
};

enum {
	ARGUMENTS_COLUMN,
	PRIORITY_COLUMN,
	ID_COLUMN,
	TASK_COLUMN,
	N_COLUMNS
};


static void
dummy_function (OAsyncWorkerTask *task)
{
	o_async_worker_task_cancel_point (task, TRUE);
}

static gpointer
dummy_func (OAsyncWorkerTask *task, gpointer arguments)
{

	usleep (400000);

	dummy_function (task);
	usleep (200000);
	
	g_print ("Dummy func had priority of %d with arguments: (%s)\n", 
		o_async_worker_task_get_priority (task),
		(gchar*) arguments);

	usleep (400000);
	o_async_worker_task_cancel_point (task, TRUE);
	usleep (200000);
		
	return (gpointer)g_strdup ("Also free me!");
}

static void
dummy_callback (OAsyncWorkerTask *task, gpointer func_result)
{
	gchar *arguments = o_async_worker_task_get_arguments (task);

	g_print ("Dummy callback going to free (%s) and (%s)\n", 
		(gchar*)arguments, (gchar*)func_result);

	g_free (arguments);
	
	if (!o_async_worker_task_is_cancelled(task))
		g_free (func_result);
}

static gboolean
foreach_task_in_tree (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	gint task_id = -1, stask_id = (gint)user_data;
	gchar *freethis = NULL;

	gtk_tree_model_get (model, iter, ID_COLUMN, &task_id, -1);

	if (task_id == stask_id)
	{

		gtk_tree_model_get (model, iter, ARGUMENTS_COLUMN, &freethis, -1);
		gtk_list_store_remove (GTK_LIST_STORE(model), iter);	

		/* In the treeview it's a copy */
		g_free (freethis);

		return TRUE;
	}

	return FALSE;
}

static void
queue_task_got_started (OAsyncWorker *queue, gint task_id, gpointer user_data)
{
	g_print ("\n\nTask with id %d just got started\n\n", task_id);
}


static void
queue_task_got_cancelled (OAsyncWorker *queue, gint task_id, gpointer user_data)
{
	g_print ("\n\nTask with id %d just got cancelled\n\n", task_id);
}

static void
queue_task_unshow_handler (OAsyncWorker *queue, gint task_id, gpointer user_data)
{
	treequeue_t *treequeue = user_data;
	GtkTreeView *treeview = treequeue->treeview;
	GtkTreeModel *model = gtk_tree_view_get_model (treeview);

	g_print ("An task needs to be removed from the treeview\n");

	gtk_tree_model_foreach (model, foreach_task_in_tree, (gpointer)task_id);

}

static void
queue_task_show_handler (OAsyncWorker *queue, gint task_id, gpointer user_data)
{
	OAsyncWorkerTask *task = o_async_worker_get_with_id (queue, task_id);
	
	if (task) {
		treequeue_t *treequeue = user_data;
		GtkTreeView *treeview = treequeue->treeview;
		
		GtkTreeIter iter;
		GtkListStore *store = GTK_LIST_STORE 
			(gtk_tree_view_get_model (treeview));

		g_print ("An task needs to be added to the treeview\n");

		gchar *arguments = o_async_worker_task_get_arguments (task);
		gint priority = o_async_worker_task_get_priority (task);

		gtk_list_store_append (store, &iter);

		g_print ("Adding task %s and id %d\n", arguments, task_id);

		gtk_list_store_set (store, &iter, ARGUMENTS_COLUMN, 
			g_strdup(arguments), -1); /* Copy! */
		gtk_list_store_set (store, &iter, PRIORITY_COLUMN, priority, -1);
		gtk_list_store_set (store, &iter, ID_COLUMN, task_id, -1);
		gtk_list_store_set (store, &iter, TASK_COLUMN, task, -1);

		g_object_unref (task);

	}
	
	return;	
}

static void
add_button_handler (GtkButton *button, gpointer user_data)
{
	treequeue_t *treequeue = user_data;
	OAsyncWorker *queue = treequeue->queue;
        OAsyncWorkerTask *dummy = o_async_worker_task_new ();
	static gint i;
	static gint prio;
	
        o_async_worker_task_set_arguments 
		(dummy, g_strdup_printf("This is a task %d", i++));
        o_async_worker_task_set_func (dummy, dummy_func);
        o_async_worker_task_set_callback (dummy, dummy_callback);
        o_async_worker_task_set_priority (dummy, prio++);

        o_async_worker_add (queue, dummy);

	g_object_unref (dummy);
}

static void
remove_button_handler (GtkButton *button, gpointer user_data)
{
	treequeue_t *treequeue = user_data;
	OAsyncWorker *queue = treequeue->queue;
	GtkTreeView *treeview = treequeue->treeview;
	GtkTreeSelection *selection = 
		gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW(treeview));
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected(selection, &model, &iter))
	{
		gint task_id=-1;
		gtk_tree_model_get (model, &iter, ID_COLUMN, &task_id, -1);
		g_print ("Trying to remove task with id %d\n", task_id);
		o_async_worker_remove (queue, task_id, TRUE);
	}
}

static void
cancel_button_handler (GtkButton *button, gpointer user_data)
{
	treequeue_t *treequeue = user_data;
	OAsyncWorker *queue = treequeue->queue;
	GtkTreeView *treeview = treequeue->treeview;
	GtkTreeSelection *selection = 
		gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW(treeview));
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected(selection, &model, &iter))
	{
		OAsyncWorkerTask *task;
		gint task_id = -1;
		
		gtk_tree_model_get (model, &iter, ID_COLUMN, &task_id, -1);		
		task = o_async_worker_get_with_id (queue, task_id);
		
		if (task) {
			o_async_worker_task_request_cancel (task);
			g_object_unref (task);
		}
	}
}


int
main (int argc, char **argv)
{
	OAsyncWorker *queue = NULL;
	GladeXML *glade = NULL;
	GtkWidget *add_button, *remove_button, 
		  *cancel_button, *treeview,
		  *window;
	treequeue_t *treequeue = g_new(treequeue_t, 1);

	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model;
	GtkTreeSelection *selection;
	
	gtk_init (&argc, &argv);
	
	glade = glade_xml_new (GLADEFILE, NULL, NULL);	

	window = glade_xml_get_widget (glade, "window");
	treeview = glade_xml_get_widget (glade, "treeview");
	add_button = glade_xml_get_widget (glade, "add_button");
	remove_button = glade_xml_get_widget (glade, "remove_button");
	cancel_button = glade_xml_get_widget (glade, "cancel_button");

	model = GTK_TREE_MODEL (gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, 
		G_TYPE_INT, G_TYPE_INT, G_TYPE_POINTER));
	gtk_tree_view_set_model (GTK_TREE_VIEW (treeview), model);
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("Job name",
			renderer, "text", ARGUMENTS_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("Priority",
			renderer, "text", PRIORITY_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
								
	
	queue = o_async_worker_new ();

	treequeue->treeview = GTK_TREE_VIEW(treeview);
	treequeue->queue = queue;

	g_signal_connect (G_OBJECT (queue), "task_started", 
		G_CALLBACK(queue_task_got_started), treequeue);
	g_signal_connect (G_OBJECT (queue), "task_finished", 
		G_CALLBACK(queue_task_unshow_handler), treequeue);
	g_signal_connect (G_OBJECT (queue), "task_added", 
		G_CALLBACK(queue_task_show_handler), treequeue);
	g_signal_connect (G_OBJECT (queue), "task_removed", 
		G_CALLBACK(queue_task_unshow_handler), treequeue);
	g_signal_connect (G_OBJECT (queue), "task_cancelled", 
		G_CALLBACK(queue_task_unshow_handler), treequeue);
	g_signal_connect (G_OBJECT (queue), "task_cancelled", 
		G_CALLBACK(queue_task_got_cancelled), treequeue);
	
	g_signal_connect (G_OBJECT (add_button), "clicked", 
		G_CALLBACK(add_button_handler), treequeue);
	g_signal_connect (G_OBJECT (remove_button), "clicked", 
		G_CALLBACK(remove_button_handler), treequeue);
	g_signal_connect (G_OBJECT (cancel_button), "clicked", 
		G_CALLBACK(cancel_button_handler), treequeue);

	g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK(gtk_exit), 0);
	
	
	gdk_threads_enter ();
	gtk_main ();
	gdk_threads_leave ();
			
	g_free (treequeue);
	
	o_async_worker_join (queue);

	g_object_unref (queue);


	return 0;
}

