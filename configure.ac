AC_INIT(liboasyncworker, 1.0, http://www.pvanhoof.be)
AC_CONFIG_SRCDIR(liboasyncworker/oasyncworker.c)
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)
AM_CONFIG_HEADER(liboasyncworker/config.h)

BASE_VERSION=1.0
AC_SUBST(BASE_VERSION)
AC_DEFINE_UNQUOTED(BASE_VERSION, "$BASE_VERSION", [Base version (Major.Minor)])

AC_CHECK_HEADER(sched.h, AC_DEFINE(HAVE_SCHED_H,,[Has sched.h]))

if test x$prefix = xNONE; then
        prefix=/usr/local
fi

AC_SUBST(prefix)

AC_PROG_CC
AM_PROG_CC_STDC
AC_HEADER_STDC
AC_PROG_LIBTOOL

PKG_CHECK_MODULES(LIBOASYNCWORKER, glib-2.0 gobject-2.0 gthread-2.0)
AC_SUBST(LIBOASYNCWORKER_CFLAGS)
AC_SUBST(LIBOASYNCWORKER_LIBS)

dnl gtk-doc
GTK_DOC_CHECK([1.0])

AC_ARG_ENABLE(vala,
	[AC_HELP_STRING([--enable-vala], [Compile with Vala bindings])],enable_vala="$enableval",enable_vala=no)

if test "x$enable_vala" = "xyes"; then
	PKG_CHECK_MODULES(VALA, [vala-1.0 >= 0.3.1])
	VAPIDIR=`$PKG_CONFIG --variable=vapidir vala-1.0`
	AC_SUBST(VAPIDIR)
	AC_PATH_PROG(VALAC, valac, no)
	if test "x$VALAC" = "xno"; then
        	AC_MSG_ERROR([Cannot find the "valac" compiler in your PATH])
	fi
	AC_PATH_PROG(VAPIGEN, vapigen, no)
	if test "x$VAPIGEN" = "xno"; then
        	AC_MSG_ERROR([Cannot find the "vapigen" binary in your PATH])
	fi
fi

AM_CONDITIONAL(ENABLE_VALA, test x$enable_vala = xyes)

dnl python bindings
AC_ARG_ENABLE(python,
            [AC_HELP_STRING([--enable-python], [Compile with python bindings])],enable_python="$enableval",enable_python=no)

if test "x$enable_python" = "xyes"; then
	AC_PATH_PROG(PYTHON, python, no)
	if test x$PYTHON = xno; then
		AC_MSG_ERROR(Please install python)
	fi
	AC_MSG_CHECKING(Python compile flags)
	changequote(<<, >>)dnl
	PY_VER=`$PYTHON -c 'import distutils.sysconfig; print distutils.sysconfig.get_config_vars("VERSION")[0];'`
	PY_LIB=`$PYTHON -c 'import distutils.sysconfig; print distutils.sysconfig.get_python_lib(standard_lib=1);'`
	PY_INC=`$PYTHON -c 'import distutils.sysconfig; print distutils.sysconfig.get_config_vars("INCLUDEPY")[0];'`
	PY_PREFIX=`$PYTHON -c 'import sys; print sys.prefix'`
	PY_EXEC_PREFIX=`$PYTHON -c 'import sys; print sys.exec_prefix'`
	changequote([, ])dnl
	if test -f $PY_INC/Python.h; then
		PYTHON_LIBS="-L$PY_LIB/config -lpython$PY_VER -lpthread -lutil"
		PYTHON_CFLAGS="-I$PY_INC"
		AC_MSG_RESULT(ok)
	else
		AC_MSG_ERROR([Can't find Python.h])
	fi
        PKG_CHECK_MODULES(PYGTK, pygtk-2.0)
	PYGTK_CODEGENDIR="`$PKG_CONFIG --variable=codegendir pygtk-2.0`"
	PYGTK_DEFSDIR="`$PKG_CONFIG --variable=defsdir pygtk-2.0`"
	AC_PATH_PROG(PYGTK_CODEGEN, pygtk-codegen-2.0, no)
	if test x$PYGTK_CODEGEN = xno; then
		AC_MSG_ERROR(Please install the application pygtk-codegen-2.0)
	fi
	AC_MSG_CHECKING(for pygtk codegendir)
	AC_MSG_RESULT($PYGTK_CODEGENDIR)
        AC_MSG_CHECKING(for pygtk defsdir)
        AC_MSG_RESULT($PYGTK_DEFSDIR)

	AC_DEFINE([ENABLE_PYTHON], [1], [Enable python bindings])
else
	PY_VER=""
	PYTHON_CFLAGS=""
	PYTHON_LIBS=""
        PYGTK_CFLAGS=""
        PYGTK_LIBS=""
	PYGTK_CODEGENDIR=""
	PYGTK_CODEGEN=""
	PYGTK_DEFSDIR=""
fi
AC_SUBST(PY_VER)
AC_SUBST(PYTHON_CFLAGS)
AC_SUBST(PYTHON_LIBS)
AC_SUBST(PYGTK_CFLAGS)
AC_SUBST(PYGTK_LIBS)
AC_SUBST(PYGTK_CODEGENDIR)
AC_SUBST(PYGTK_DEFSDIR)
AM_CONDITIONAL(ENABLE_PYTHON, test x$enable_python = xyes)

dnl csharp bindings
AC_ARG_ENABLE(csharp,
            [AC_HELP_STRING([--enable-csharp], [Compile with csharp bindings])],enable_csharp="$enableval",enable_csharp=no)

if test "x$enable_csharp" = "xyes"; then
	PKG_CHECK_MODULES(MONO_DEPENDENCY, mono, has_mono=true, has_mono=false)

	if test "x$has_mono" = "xtrue"; then
		AC_PATH_PROG(RUNTIME, mono, no)
		AC_PATH_PROG(CSC, mcs, no)
		if test `uname -s` = "Darwin"; then
			LIB_PREFIX=
			LIB_SUFFIX=.dylib
		else
			LIB_PREFIX=.so
			LIB_SUFFIX=
		fi
	else
		AC_PATH_PROG(CSC, csc.exe, no)
		if test x$CSC = "xno"; then
			AC_MSG_ERROR([You need to install either mono or .Net])
		else
			RUNTIME=
			LIB_PREFIX=
			LIB_SUFFIX=.dylib
		fi
	fi


	PKG_CHECK_MODULES(GTK_SHARP, gtk-sharp-2.0 >= 1.0.4)
	PKG_CHECK_MODULES(GAPI, gapi-2.0 >= 1.9.0)

	AC_PATH_PROG(GACUTIL, gacutil, no)
	if test "x$GACUTIL" = "xno" ; then
	        AC_MSG_ERROR([No gacutil tool found])
	fi

	winbuild=no
	case "$host" in
 	      *-*-mingw*|*-*-cygwin*)
     	          winbuild=yes
      	         ;;
	esac
	if test "x$winbuild" = "xyes" ; then
		AC_PATH_PROG(PARSER, gapi-parser.exe, no)
		AC_PATH_PROG(CODEGEN, gapi-codegen.exe, no)
		AC_PATH_PROG(FIXUP, gapi-fixup.exe, no)
	else
		AC_PATH_PROG(PARSER, gapi2-parser, no)
		AC_PATH_PROG(CODEGEN, gapi2-codegen, no)
		AC_PATH_PROG(FIXUP, gapi2-fixup, no)
	fi

	if test "x$PARSER" = "xno" ; then
		AC_MSG_ERROR([No gapi-parser tool found])
	fi

	if test "x$CODEGEN" = "xno" ; then
		AC_MSG_ERROR([No gapi-codegen tool found])
	fi

	if test "x$FIXUP" = "xno" ; then
		AC_MSG_ERROR([No gapi-fixup tool found])
	fi

	AC_DEFINE([ENABLE_CSHARP], [1], [Enable csharp bindings])
else
	CODEGEN=""
	FIXUP=""
	GACUTIL=""
	GAPI_CFLAGS=""
	GAPI_LIBS=""
	LIB_PREFIX=""
	LIB_SUFFIX=""
	GTK_SHARP_CFLAGS=""
	GTK_SHARP_LIBS=""
	RUNTIME=""
	CSC=""
	GAPI2_CODEGEN=""
	GAPI2_FIXUP=""
fi
AC_SUBST(CODEGEN)
AC_SUBST(FIXUP)
AC_SUBST(GAPI_CFLAGS)
AC_SUBST(GAPI_LIBS)
AC_SUBST(LIB_PREFIX)
AC_SUBST(LIB_SUFFIX)
AC_SUBST(GTK_SHARP_CFLAGS)
AC_SUBST(GTK_SHARP_LIBS)
AC_SUBST(CSC)
AC_SUBST(GAPI2_CODEGEN)
AC_SUBST(GAPI2_FIXUP)
AC_SUBST(RUNTIME)
AC_SUBST(GACUTIL)
AM_CONDITIONAL(ENABLE_CSHARP, test x$enable_csharp = xyes)

BINDIR=$prefix/bin
AC_SUBST(BINDIR)

AC_OUTPUT([
Makefile
liboasyncworker/Makefile
include/Makefile
include/oasyncworker/Makefile
docs/Makefile
docs/reference/Makefile
bindings/Makefile
bindings/python/Makefile
bindings/csharp/Makefile
bindings/csharp/oasyncworker-1.0.dll.config
bindings/csharp/AssemblyInfo.cs
bindings/vala/Makefile
oasyncworker.pc
])

